<?php

use Xstudios\Stringutil\StringUtil;

class StringUtilTest extends PHPUnit_Framework_TestCase
{

    public function testWords()
    {
        $str = StringUtil::words('This is a test of the word limiter.', 4);
        $this->assertEquals('This is a test...', $str);
    }

    public function testLimit()
    {
        $str = StringUtil::limit('This is a test of the char limiter.', 15);
        $this->assertEquals('This is a test...', $str);
    }

    public function testRandom()
    {
        $str = StringUtil::random(16);
        $this->assertNotEmpty($str);
        $this->assertEquals(16, mb_strlen($str));
    }

    public function testUpper()
    {
        $str = StringUtil::upper('This is a Test');
        $this->assertEquals('THIS IS A TEST', $str);
    }

    public function testLower()
    {
        $str = StringUtil::lower('This is a Test', 15);
        $this->assertEquals('this is a test', $str);
    }

    public function testBetween()
    {
        $str = StringUtil::between('String between foo and bar baz', 'between', 'baz');
        $this->assertEquals('foo and bar', $str);
    }

    public function testStripLineBreaks()
    {
        $str = StringUtil::stripLineBreaks("Line break here\nand here\r");
        $this->assertEquals("Line break here and here", $str);
    }

    public function testTrimSlashes()
    {
        $str = StringUtil::trimSlashes('/no/trailing/slashes/');
        $this->assertEquals('no/trailing/slashes', $str);
    }

    public function testStripBackSlashes()
    {
        $str = StringUtil::stripBackSlashes("O\'reilly");
        $this->assertEquals("O'reilly", $str);
    }

    public function testStripQuotes()
    {
        $str = StringUtil::stripQuotes('Say, "Hello World!"');
        $this->assertEquals("Say, Hello World!", $str);

        $str = StringUtil::stripQuotes("Say, 'Hello World!'");
        $this->assertEquals("Say, Hello World!", $str);
    }

    public function testQuotesToEntities()
    {
        // Single quote
        $str = StringUtil::quotesToEntities("That's great!");
        $this->assertEquals("That&#39;s great!", $str);

        // Smart quote
        $str = StringUtil::quotesToEntities("That’s great!");
        $this->assertEquals("That&#39;s great!", $str);

        // Double quotes
        $str = StringUtil::quotesToEntities("\"Hello World!\"");
        $this->assertEquals("&quot;Hello World!&quot;", $str);

        // Smart quotes
        $str = StringUtil::quotesToEntities("”Hello World!”");
        $this->assertEquals("&quot;Hello World!&quot;", $str);
    }

    public function testCleanWordString()
    {
        // Smart quotes
        $str = StringUtil::cleanWordString("That’s great! ”Hello World!”");
        $this->assertEquals("That's great! \"Hello World!\"", $str);
    }

    public function testReduceDoubleSlashes()
    {
        $str = StringUtil::reduceDoubleSlashes("path/to//a/folder//");
        $this->assertEquals("path/to/a/folder/", $str);
    }

    public function testToBoolean()
    {
        $str = StringUtil::toBoolean("1");
        $this->assertEquals(true, $str);

        $str = StringUtil::toBoolean("true");
        $this->assertEquals(true, $str);

        $str = StringUtil::toBoolean("on");
        $this->assertEquals(true, $str);

        $str = StringUtil::toBoolean("yes");
        $this->assertEquals(true, $str);

        $str = StringUtil::toBoolean("active");
        $this->assertEquals(true, $str);

        $str = StringUtil::toBoolean("enabled");
        $this->assertEquals(true, $str);

        $str = StringUtil::toBoolean("0");
        $this->assertEquals(false, $str);

        $str = StringUtil::toBoolean("false");
        $this->assertEquals(false, $str);

        $str = StringUtil::toBoolean("off");
        $this->assertEquals(false, $str);

        $str = StringUtil::toBoolean("no");
        $this->assertEquals(false, $str);

        $str = StringUtil::toBoolean("inactive");
        $this->assertEquals(false, $str);

        $str = StringUtil::toBoolean("disabled");
        $this->assertEquals(false, $str);
    }

    public function testRemoveSpecialChars()
    {
        $str = StringUtil::stripSpecial("Tim is a piece of ~!@#$%^&*()_-+={}[]|\\:;\"<>,.?/`");
        $this->assertEquals("Tim is a piece of -", $str);
    }

    public function testCleanNumber()
    {
        $str = StringUtil::cleanNumber("(123) 456-7890");
        $this->assertEquals("1234567890", $str);

        $str = StringUtil::cleanNumber("123.456.7890", false);
        $this->assertEquals("1234567890", $str);
    }

    public function testContains()
    {
        $str = StringUtil::contains("foo contains bar", "bar");
        $this->assertEquals(true, $str);
    }

    public function testStartsWith()
    {
        $str = StringUtil::startsWith("foo contains bar", "foo");
        $this->assertEquals(true, $str);
    }

    public function testEndsWith()
    {
        $str = StringUtil::endsWith("foo contains bar", "bar");
        $this->assertEquals(true, $str);
    }

    public function testFinish()
    {
        $str = StringUtil::finish("path/to/folder//", "/");
        $this->assertEquals("path/to/folder/", $str);

        $str = StringUtil::finish("path/to/folder", "/");
        $this->assertEquals("path/to/folder/", $str);
    }

    public function testIs()
    {
        $str = StringUtil::is("foo * bar", "foo contains bar");
        $this->assertEquals(true, $str);
    }

    public function testStudly()
    {
        $str = StringUtil::studly("My class name");
        $this->assertEquals("MyClassName", $str);
    }

    public function testCamel()
    {
        $str = StringUtil::camel("My class name");
        $this->assertEquals("myClassName", $str);
    }

    public function testSnake()
    {
        $str = StringUtil::snake("MyClassName");
        $this->assertEquals("my_class_name", $str);
    }

    public function testSlug()
    {
        $str = StringUtil::slug("This is my title for #1");
        $this->assertEquals("this-is-my-title-for-1", $str);

        // Test Spanish accent chars
        $str = StringUtil::slug("á, é, í, ó, ú, ü, ñ, ¿, ¡");
        $this->assertEquals("a-e-i-o-u-u-n", $str);
    }

}
