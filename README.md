# String Util

## Overview
Laravel 4 package for easily adding CSS active states.

## Installation
Open your `composer.json` file and add the following to the `repositories` array:

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/xstudios/php-stringutil.git"
        }
    ],

The add the folllowing to the `require` object:

    "xstudios/php-stringutil": "v0.0.1"

### Install the dependencies

    php composer install

Once the package is installed you need to register the service provider with the application. Open up `app/config/app.php` and find the `providers` key.

    'providers' => array(
        # Other providers here ...
        'Xstudios\Stringutil\StringUtilServiceProvider',
    )

## Contribute
In lieu of a formal styleguide, take care to maintain the existing coding style.

## License
MIT License (c) [X Studios](http://xstudiosinc.com)
