<?php namespace Xstudios\Stringutil;

/**
 * X Studios String Util
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author       Tim Santor <tsantor@xstudios.agency>
 * @link         http://www.xstudios.agency
 */

class StringUtil {

    // ------------------------------------------------------------------------

    /**
     * Transliterate a UTF-8 value to ASCII.
     *
     * @param  string  $str
     * @return string
     */
    public static function ascii($str)
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Word Limiter
     *
     * @param  string  $str
     * @param  int     $limit
     * @param  string  $end
     * @return string
     */
    public static function words($str, $limit, $end = '...')
    {
        preg_match('/^\s*+(?:\S++\s*+){1,'.$limit.'}/u', $str, $matches);

        if ( ! isset($matches[0])) return $str;

        if (strlen($str) == strlen($matches[0])) return $str;

        return trim($matches[0]).$end;
    }

    // ------------------------------------------------------------------------

    /**
     * Character Limiter
     *
     * Limits the string based on the character count.  Preserves complete words
     * so the character count may not be exactly as specified.
     *
     * @param  string  $str
     * @param  int     $limit
     * @param  string  $end
     * @return string
     */

    public static function limit($str, $limit, $end='...')
    {
        if (mb_strlen($str) < $limit) return $str;

        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

        if (mb_strlen($str) <= $limit) return $str;

        $out = "";
        foreach (explode(' ', trim($str)) as $val)
        {
            $out .= $val.' ';

            if (mb_strlen($out) >= $limit)
            {
                $out = trim($out);
                return (mb_strlen($out) == mb_strlen($str)) ? $out : $out.$end;
            }
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int  $length
     * @return string
     *
     * @throws \RuntimeException
     */
    public static function random($length = 16)
    {
        if (function_exists('openssl_random_pseudo_bytes'))
        {
            $bytes = openssl_random_pseudo_bytes($length * 2);

            if ($bytes === false)
            {
                throw new \RuntimeException('Unable to generate random string.');
            }

            return substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, $length);
        }

        return static::quickRandom($length);
    }

    // ------------------------------------------------------------------------

    /**
     * Return the length of the given string.
     *
     * @param  string  $str
     * @return int
     */
    public static function length($str)
    {
        return mb_strlen($str);
    }

    // ------------------------------------------------------------------------

    /**
     * Convert the given string to upper-case.
     *
     * @param  string  $str
     * @return string
     */
    public static function upper($str)
    {
        return mb_strtoupper($str);
    }

    // ------------------------------------------------------------------------

    /**
     * Convert the given string to lower-case.
     *
     * @param  string  $str
     * @return string
     */
    public static function lower($str)
    {
        return mb_strtolower($str);
    }

    // ------------------------------------------------------------------------

    /**
     * Convert the given string to title case.
     *
     * @param  string  $str
     * @return string
     */
    public static function title($str)
    {
        return mb_convert_case($str, MB_CASE_TITLE, 'UTF-8');
    }

    // ------------------------------------------------------------------------

    /**
     * Get string between 2 strings
     *
     * @param     string $str a string
     * @param     string $start a string
     * @param     string $end a string
     * @return    string
     */
    public static function between($str, $start, $end)
    {
        $str = " ".$str;
        $ini = strpos($str,$start);
        if ($ini == 0) return "";
        $ini += mb_strlen($start);
        $len = strpos($str,$end,$ini) - $ini;
        return trim(substr($str,$ini,$len));
    }

    // ------------------------------------------------------------------------

    /**
     * Strip Line Breaks
     *
     * Strips line breaks such as \n and \r
     *
     * @param     string $str a string
     * @return    string
     */
    public static function stripLineBreaks($str)
    {
        $str = str_replace(array("\n", "\r")," ", $str);
        return trim($str);
    }

    // ------------------------------------------------------------------------

    /**
     * Trim Slashes
     *
     * Removes any leading/trailing slashes from a string
     *
     * @param     string
     * @return    string
     */
    public static function trimSlashes($str)
    {
        return trim($str, '/');
    }

    // ------------------------------------------------------------------------

    /**
     * Strip Back Slashes (eg - \)
     *
     * Removes slashes contained in a string or in an array
     *
     * @param     mixed string or array
     * @return    mixed string or array
     */
    public static function stripBackSlashes($str)
    {
        if (is_array($str))
        {
            foreach ($str as $key => $val)
            {
                $str[$key] = strip_slashes($val);
            }
        }
        else
        {
            $str = stripslashes($str);
        }

        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Strip Quotes
     *
     * Removes single and double quotes from a string
     *
     * @param     string
     * @return    string
     */
    public static function stripQuotes($str)
    {
        $str = self::cleanWordString($str);
        return str_replace(array('"', "'"), '', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Quotes to Entities
     *
     * Converts single and double quotes to entities
     *
     * @param     string
     * @return    string
     */
    public static function quotesToEntities($str)
    {
        $str = self::cleanWordString($str);
        return str_replace(array("'","\""), array("&#39;","&quot;"), $str);
    }
    // ------------------------------------------------------------------------

    /**
     * Clean MS Word string
     *
     * Replace Microsoft Word version of single and double quotations marks
     * (“ ” ‘ ’) with regular quotes (' and ")
     *
     * @param     string
     * @return    string
     */
    public static function cleanWordString($str)
    {
        return iconv(mb_detect_encoding($str), 'ASCII//TRANSLIT', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Reduce Double Slashes
     *
     * Converts double slashes in a string to a single slash,
     * except those found in http://
     *
     * @param     string
     * @return    string
     */
    public static function reduceDoubleSlashes($str)
    {
        return preg_replace("#(^|[^:])//+#", "\\1/", $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Returns a boolean for typical boolean strings
     *
     * "1", "true",  "on"  and "yes" will return true
     * "0", "false", "off" and "no"  will return false
     *
     * @param     string
     * @return    boolean
     */
    public static function toBoolean($str)
    {
        switch (mb_strtolower($str)) {
            case '1':
            case 'true':
            case 'on':
            case 'yes':
            case 'active':
            case 'enabled':
                return true;
                break;
            case '0':
            case 'false':
            case 'off':
            case 'no':
            case 'inactive':
            case 'disabled':
                return false;
                break;
            default:
                return false;
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Remove all special characters but a hyphen unless specified.
     *
     * @param     string
     * @return    string
     */
    public static function stripSpecial($str, $include_hyphen=false)
    {
        $str = preg_replace('/[^A-Za-z0-9-\s]/', '', $str);
        if ($include_hyphen)
        {
            $str = str_replace('-', '', $str);
        }
        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Return only numbers.
     *
     * Money, social security numbers , phone numbers, etc.
     *
     * @param     string
     * @return    string
     */
    public static function cleanNumber($str, $allow_decimal=true)
    {
        if ($allow_decimal)
        {
            $str = preg_replace('/[^0-9\.]/', '', $str);
        }
        else
        {
            $str = preg_replace('/[^0-9]/', '', $str);
        }
        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Determine if a given string contains a given substring.
     *
     * @param  string  $haystack
     * @param  string|array  $needles
     * @return bool
     */
    public static function contains($haystack, $needles)
    {
        foreach ((array) $needles as $needle)
        {
            if ($needle != '' && strpos($haystack, $needle) !== false) return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string  $haystack
     * @param  string|array  $needles
     * @return bool
     */
    public static function startsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle)
        {
            if ($needle != '' && strpos($haystack, $needle) === 0) return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Determine if a given string ends with a given substring.
     *
     * @param  string  $haystack
     * @param  string|array  $needles
     * @return bool
     */
    public static function endsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle)
        {
            if ((string) $needle === substr($haystack, -strlen($needle))) return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Cap a string with a single instance of a given str.
     *
     * @param  string  $str
     * @param  string  $cap
     * @return string
     */
    public static function finish($str, $cap)
    {
        $quoted = preg_quote($cap, '/');

        return preg_replace('/(?:'.$quoted.')+$/', '', $str).$cap;
    }

    // ------------------------------------------------------------------------

    /**
     * Determine if a given string matches a given pattern.
     *
     * @param  string  $pattern
     * @param  string  $str
     * @return bool
     */
    public static function is($pattern, $str)
    {
        if ($pattern == $str) return true;

        $pattern = preg_quote($pattern, '#');

        // Asterisks are translated into zero-or-more regular expression wildcards
        // to make it convenient to check if the strings starts with the given
        // pattern such as "library/*", making any string check convenient.
        $pattern = str_replace('\*', '.*', $pattern).'\z';

        return (bool) preg_match('#^'.$pattern.'#', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Convert a str to studly caps case.
     *
     * @param  string  $str
     * @return string
     */
    public static function studly($str)
    {
        $str = ucwords(str_replace(array('-', '_'), ' ', $str));

        return str_replace(' ', '', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Convert a string to camel case.
     *
     * @param  string  $str
     * @return string
     */
    public static function camel($str)
    {
        return lcfirst(static::studly($str));
    }

    // ------------------------------------------------------------------------

    /**
     * Convert a string to snake case.
     *
     * @param  string  $str
     * @param  string  $delimiter
     * @return string
     */
    public static function snake($str, $delimiter = '_')
    {
        $replace = '$1'.$delimiter.'$2';

        return ctype_lower($str) ? $str : mb_strtolower(preg_replace('/(.)([A-Z])/', $replace, $str));
    }

    // ------------------------------------------------------------------------

    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @return string
     */
    public static function slug($title, $separator = '-')
    {
        $title = static::ascii($title);
        $title = static::cleanWordString($title);

        // Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

        return trim($title, $separator);
    }

    // ------------------------------------------------------------------------

}

/* End of file */
